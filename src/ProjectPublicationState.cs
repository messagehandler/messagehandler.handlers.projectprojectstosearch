namespace ProjectProjectsToSearch
{
    public enum ProjectPublicationState
    {
        Draft,
        Published
    }
}
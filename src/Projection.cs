using MessageHandler;
using MessageHandler.SDK.EventSource;
using MessageHandler.Contracts.Projects;

namespace ProjectProjectsToSearch
{
    public class Projection :
        IProjection<SearchEntry, ProjectDrafted>,
        IProjection<SearchEntry, ProjectPublished>,
        IProjection<SearchEntry, ProjectUnpublished>,
        IProjection<SearchEntry, ProjectUpdated>,
        IProjection<SearchEntry, ProjectRenamed>,
        IProjection<SearchEntry, ProjectObsoleted>
    {
        public void ProjectInternal(SearchEntry record, dynamic msg)
        {
            record.Filter1 = msg.Details.Author;
            record.User = msg.Details.AuthorId;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.Description;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.State1 = msg.Details.PublicationState.ToString();
            record.State2 = msg.Details.State.ToString();
            record.Id = msg.Details.ProjectId;
            record.Tenant = msg.Details.TenantId;
            record.Object = Json.Encode(msg.Details);
            record.Type = "Project";
        }

        public void Project(SearchEntry record, ProjectDrafted msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, ProjectPublished msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, ProjectUnpublished msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, ProjectUpdated msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, ProjectRenamed msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, ProjectObsoleted msg)
        {
            ProjectInternal(record, msg);
        }
    }
}
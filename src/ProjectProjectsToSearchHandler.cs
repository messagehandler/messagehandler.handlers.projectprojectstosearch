﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using MessageHandler;
using MessageHandler.SDK.EventSource;
using Yaus.Framework.AzureSearch;
using Environment = MessageHandler.Environment;

namespace ProjectProjectsToSearch
{
    public class ProjectProjectsToSearchHandler :
        IStandingQuery<DynamicJsonObject>,
        IAction<DynamicJsonObject>
    {
        private readonly AzureSearchClient _searchclient;
        private readonly string _sourceType;
        private readonly string _index;
        private readonly MemoryCache _projections = new MemoryCache("Projections");
        private readonly IInvokeProjections _projectionInvoker;

        public ProjectProjectsToSearchHandler(IConfigurationSource source, IVariablesSource variables, ITemplatingEngine templating, IInvokeProjections projectionInvoker)
        {
            _projectionInvoker = projectionInvoker;

            dynamic channelVariables = variables.GetChannelVariables(Channel.Current());
            dynamic accountVariables = variables.GetAccountVariables(Account.Current());
            dynamic environmentVariables = variables.GetEnvironmentVariables(Environment.Current());

            var config = source.GetConfiguration<ProjectProjectsToSearchConfig>();

            var serviceName = templating.Apply(config.SearchServiceName, null, channelVariables, environmentVariables, accountVariables);
            var apiKey = templating.Apply(config.SearchServiceApiKey, null, channelVariables, environmentVariables, accountVariables);
            _sourceType = templating.Apply(config.SourceType, null, channelVariables, environmentVariables, accountVariables);
            _index = templating.Apply(config.SearchServiceIndex, null, channelVariables, environmentVariables, accountVariables);

            _searchclient = new AzureSearchClient(serviceName, apiKey);
        }

        public IObservable<DynamicJsonObject> Compose(IObservable<DynamicJsonObject> messages)
        {
            return from e in messages
                   where IsProjectEvent(e)
                   select e;
        }

        private bool IsProjectEvent(dynamic e)
        {
            string what = e.What;
            string projectId = e.ProjectId;
            string sourceId = e.SourceId;
            string sourceType = e.SourceType;

            Trace.TraceInformation("Evaluating What:'{0}', ProjectId:'{1}', SourceId:'{2}', SourceType:'{3}'", what, projectId, sourceId, sourceType);

            return e.What != null && e.ProjectId != null && e.SourceId != null && e.SourceType == _sourceType;
        }

        public async Task Action(DynamicJsonObject t)
        {
            dynamic m = t;

            string projectId = m.ProjectId;
            string what = m.What;

            Trace.TraceInformation("Received event from project '{0}'.", projectId);

            var eventType = GetType(what);
            if (eventType == null)
            {
                Trace.TraceInformation("Received event of unknown type '{0}' skipping", what);
                return;
            }

            var @event = Json.Decode(Json.Encode(t), eventType);
            if (@event == null)
            {
                Trace.TraceInformation("Could not deserialize json into claimed type '{0}'", what);
                return;
            }

            var lazy = new Lazy<SearchEntry>(() => _searchclient.Get<SearchEntry>(_index, projectId).Result ?? new SearchEntry() { Id = projectId });
            var cachedLazy = (Lazy<SearchEntry>)_projections.AddOrGetExisting(projectId, lazy, new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromMinutes(1) });
            var project = (cachedLazy ?? lazy).Value;

            Trace.TraceInformation("Retrieved project '{0}', going to apply projection.", projectId);

            _projectionInvoker.Invoke(project, @event);

            Trace.TraceInformation("Projection applied to project '{0}', persisting.", projectId);
        }

        private static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.ExportedTypes.FirstOrDefault(t => t.Name == typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

        public async Task Complete()
        {
            foreach (var pair in _projections)
            {
                var project = ((Lazy<SearchEntry>)pair.Value).Value;
                await _searchclient.Upsert(_index, new object[] { project });
            }
        }

    }
}